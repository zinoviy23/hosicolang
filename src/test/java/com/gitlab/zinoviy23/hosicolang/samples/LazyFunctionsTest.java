package com.gitlab.zinoviy23.hosicolang.samples;

import com.gitlab.zinoviy23.hosicolang.LazyValue;
import org.junit.Test;

import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests for some functions <br>
 *     It is shows, how functions will be look in Java. <br>
 *     Also it shows, why it is necessary to optimize recursion.
 */
public class LazyFunctionsTest {
    /**
     * Function for sum of two values
     */
    private static Function<LazyValue<Integer>, Function<LazyValue<Integer>, LazyValue<Integer>>> sum =
            (x) -> (y) -> new LazyValue<>(() -> x.getValue() + y.getValue());

    /**
     * Function for subtract of two values
     */
    private static Function<LazyValue<Integer>, Function<LazyValue<Integer>, LazyValue<Integer>>> sub =
            (x) -> (y) -> new LazyValue<>(() -> x.getValue() - y.getValue());

    /**
     * Function for product of two values
     */
    private static Function<LazyValue<Integer>, Function<LazyValue<Integer>, LazyValue<Integer>>> prod =
            (x) -> (y) -> new LazyValue<>(() -> x.getValue() * y.getValue());


    /**
     * Lazy factorial
     */
    private static Function<LazyValue<Integer>, LazyValue<Integer>> factorial =
            (n) -> n.getValue() == 0
                    ? new LazyValue<>(() -> 1)
                    : prod.apply(n).apply(getFactorial().apply(sub.apply(n).apply(new LazyValue<>(() -> 1))));

    /**
     * Getter for factorial field
     * @return factorial
     */
    private static Function<LazyValue<Integer>, LazyValue<Integer>> getFactorial() {
        return factorial;
    }

    /**
     * Fast factorial
     */
    @SuppressWarnings("ConditionalBreakInInfiniteLoop")
    private static Function<LazyValue<Integer>, LazyValue<Integer>> fastFactorial = (n) -> {
        int calculatedN = n.getValue();
        int acc = 1;

        while (true) {
            if (calculatedN == 0)
                break;

            acc *= calculatedN;
            calculatedN = calculatedN - 1;
        }

        final int result = acc;
        return new LazyValue<>(() -> result);
    };

    /**
     * Tests sum function
     */
    @Test
    public void testsSum() {
        LazyValue<Integer> res = sum.apply(new LazyValue<>(() -> 3)).apply(new LazyValue<>(() -> 5));

        assertEquals(8, (int) res.getValue());
    }

    /**
     * Tests subtract
     */
    @Test
    public void testSub() {
        LazyValue<Integer> res = sub.apply(new LazyValue<>(() -> 3)).apply(new LazyValue<>(() -> 5));

        assertEquals(-2, (int) res.getValue());
    }

    /**
     * Tests product
     */
    @Test
    public void testProduct() {
        LazyValue<Integer> res = prod.apply(new LazyValue<>(() -> 3)).apply(new LazyValue<>(() -> 5));

        assertEquals(15, (int) res.getValue());
    }

    /**
     * Tests factorial
     */
    @Test
    public void testFactorialSimple() {
        LazyValue<Integer> res = factorial.apply(new LazyValue<>(() -> 6));

        assertEquals(720, (int) res.getValue());
    }

    /**
     * Tests falling with StackOverflowError
     */
    @Test(expected = StackOverflowError.class)
    public void testFactorialEx() {
        LazyValue<Integer> res = factorial.apply(new LazyValue<>(() -> 1000000));

        assertNotNull(res);
    }

    /**
     * Tests fast factorial
     */
    @Test
    public void testFastFactorial() {
        LazyValue<Integer> res = fastFactorial.apply(new LazyValue<>(() -> 6));

        assertEquals(720, (int) res.getValue());
    }

    /**
     * Tests, that fast factorial doesn't throw the StackOverflowError
     */
    @Test
    public void testFastFactorialWithoutException() {
        LazyValue<Integer> res = fastFactorial.apply(new LazyValue<>(() -> 1000000));

        assertNotNull(res.getValue());
    }
}
