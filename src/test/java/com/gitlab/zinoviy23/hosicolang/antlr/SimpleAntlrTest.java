package com.gitlab.zinoviy23.hosicolang.antlr;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.junit.After;
import org.junit.Test;

import java.util.BitSet;

import static org.junit.Assert.*;

/**
 * Simple tests of antlr grammar
 */
public class SimpleAntlrTest {
    /**
     * Gets parser with applied string
     * @param stringToParse string, which will be parsed
     * @return parser with applied string
     */
    private static HosicoLangParser getParserByString(String stringToParse) {
        HosicoLangLexer lexer = new HosicoLangLexer(CharStreams.fromString(stringToParse));
        CommonTokenStream stream = new CommonTokenStream(lexer);
        HosicoLangParser parser = new HosicoLangParser(stream);

        parser.addErrorListener(new ANTLRErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object o, int i, int i1, String s, RecognitionException e) {
                System.out.println(s);
                throw new RuntimeException("Syntax Error!");
            }

            @Override
            public void reportAmbiguity(Parser parser, DFA dfa, int i, int i1, boolean b, BitSet bitSet, ATNConfigSet atnConfigSet) {
                System.out.println("ambiguity in " + parser.getTokenStream().getText());
            }

            @Override
            public void reportAttemptingFullContext(Parser parser, DFA dfa, int i, int i1, BitSet bitSet, ATNConfigSet atnConfigSet) {

            }

            @Override
            public void reportContextSensitivity(Parser parser, DFA dfa, int i, int i1, int i2, ATNConfigSet atnConfigSet) {

            }
        });

        return parser;
    }

    @After
    public void tearDown() {
        HosicoLangParser.clearModuleMethods();
        HosicoLangParser.unsetCurrentModuleName();
    }

    /**
     * Tests, that syntax errors exists
     */
    @SuppressWarnings("unused")
    @Test(expected = RuntimeException.class)
    public void syntaxError() {
        HosicoLangParser.ExpressionContext context = getParserByString("**").expression();
    }

    /**
     * Tests parsing of simple expression
     */
    @Test
    public void parseSimpleExpression() {
        HosicoLangParser.ExpressionContext root = getParserByString("1 + 2 - 3").expression();

        assertNotNull(root);

        assertNotNull(root.MINUS());
        assertNotNull(root.expression(1).simpleExpression());
        assertEquals("3", root.expression(1).simpleExpression().getText());
        assertNotNull(root.expression(0).PLUS());
        assertNotNull(root.expression(0).expression(0).simpleExpression());
        assertNotNull(root.expression(0).expression(1).simpleExpression());
        assertEquals("1", root.expression(0).expression(0).simpleExpression().getText());
        assertEquals("2", root.expression(0).expression(1).simpleExpression().getText());
    }

    /**
     * Tests function definition
     */
    @Test
    public void parseFuncDef() {
        HosicoLangParser.FuncDefContext root = getParserByString("f a b = a + b;").funcDef(true);

        assertEquals("default", HosicoLangParser.getCurrentModuleName());
//        System.out.println(HosicoLangParser.getModuleMethods());
        assertTrue(HosicoLangParser.getModuleMethods().get("default").containsKey("f"));

        assertNotNull(root);

        assertEquals(3, root.ID().size());
        assertEquals("f", root.ID(0).getText());
        assertEquals("a", root.ID(1).getText());
        assertEquals("b", root.ID(2).getText());
        assertNotNull(root.expression());
        assertNull(root.WHEREKW());
        assertEquals( 2, root.expression().expression().size());
        assertNotNull(root.expression().expression(0).simpleExpression());
        assertNotNull(root.expression().expression(1).simpleExpression());
        assertNotNull(root.expression().PLUS());
        assertEquals("a", root.expression().expression(0).simpleExpression().getText());
        assertEquals("b", root.expression().expression(1).simpleExpression().getText());
    }

    /**
     * Tests function definition with where
     */
    @Test
    public void parseFundDefWithWhere() {
        HosicoLangParser.FuncDefContext root = getParserByString("f a = ff a where {ff x = x;};").funcDef(true);

        assertNotNull(root);
        assertNotNull(root.WHEREKW());
        assertEquals(1, root.funcDef().size());
        assertEquals(2, root.funcDef(0).ID().size());
        assertEquals("ff", root.funcDef(0).ID(0).getText());
        assertEquals("x", root.funcDef(0).ID(1).getText());
    }

    /**
     * Tests parsing of if
     */
    @Test
    public void parseIf() {
        HosicoLangParser.ExpressionContext root = getParserByString("if true then 1 else 2").expression();

        assertNotNull(root);
        assertNotNull(root.IFKW());
        assertNotNull(root.ELSEKW());
        assertEquals(3, root.expression().size());
        assertNotNull(root.expression(0).simpleExpression());
        assertNotNull(root.expression(1).simpleExpression());
        assertNotNull(root.expression(2).simpleExpression());
        assertNotNull(root.expression(0).simpleExpression().BOOLEAN());
        assertNotNull(root.expression(1).simpleExpression().INT());
        assertNotNull(root.expression(2).simpleExpression().INT());
        assertEquals("true", root.expression(0).simpleExpression().BOOLEAN().getText());
        assertEquals("1", root.expression(1).simpleExpression().INT().getText());
        assertEquals("2", root.expression(2).simpleExpression().INT().getText());
    }

    /**
     * Tests let clause
     */
    @Test
    public void parseLet() {
        HosicoLangParser.ExpressionContext root = getParserByString("let {a x = 10;} in a 20").expression();

        assertNotNull(root);
        assertNotNull(root.LETKW());
        assertNotNull(root.INKW());
        assertNotNull(root.LBRACE());
        assertNotNull(root.RBRACE());
        assertEquals(1, root.funcDef().size());
        assertNotNull(root.expression(0));
        assertNotNull(root.expression(0).functionCall());
    }

    /**
     * Test where and currentModuleName field
     */
    @Test
    public void parseModuleName() {
        HosicoLangParser.ProgramContext root = getParserByString("module KekLolAAA where").program();

        assertNotNull(root);
        assertNotNull(root.ID());
        assertEquals("KekLolAAA", root.ID().getText());
        assertEquals("KekLolAAA", HosicoLangParser.getCurrentModuleName());
    }

    /**
     * Tests, that it throws exception, when two methods with same names defined
     */
    @Test(expected = RuntimeException.class)
    public void defineFunctionTwiceException() {
        HosicoLangParser.ProgramContext root = getParserByString(
                "module Main where\n" +
                "f x y = x + y;\n" +
                        "f x y = y - x;").program();
    }
}
