// Generated from /home/sanusha/Proga/lazyjava/HosicoLang.g4 by ANTLR 4.7
package com.gitlab.zinoviy23.hosicolang.antlr;

import java.util.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class HosicoLangParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		IFKW=1, ELSEKW=2, THENKW=3, WHEREKW=4, LETKW=5, INKW=6, IMPORTKW=7, MODULEKW=8, 
		DATAKW=9, INTERFACEKW=10, IMPLEMENTSKW=11, PLUS=12, MINUS=13, LBRACE=14, 
		RBRACE=15, MULT=16, DIV=17, LS=18, GR=19, LE=20, GE=21, EQ=22, NOTEQ=23, 
		ASSIGN=24, OR=25, DELIMETER=26, FUNCTION_ARROW=27, TYPEDOTS=28, COMMA=29, 
		DOT=30, FUNCTION_TO_OPERATION=31, AND=32, LPAR=33, RPAR=34, LSQBR=35, 
		RSQBR=36, SEMICOLON=37, BOOLEAN=38, INT=39, FLOAT=40, STRING=41, ID=42, 
		SKIP_=43;
	public static final int
		RULE_program = 0, RULE_importStatements = 1, RULE_funcDef = 2, RULE_funcTypeDef = 3, 
		RULE_typeExpression = 4, RULE_qualifiedId = 5, RULE_expression = 6, RULE_simpleExpression = 7, 
		RULE_dataDef = 8, RULE_ctorDef = 9, RULE_fieldDef = 10, RULE_functionCall = 11, 
		RULE_argument = 12, RULE_interfaceDef = 13, RULE_implementsDef = 14;
	public static final String[] ruleNames = {
		"program", "importStatements", "funcDef", "funcTypeDef", "typeExpression", 
		"qualifiedId", "expression", "simpleExpression", "dataDef", "ctorDef", 
		"fieldDef", "functionCall", "argument", "interfaceDef", "implementsDef"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'if'", "'else'", "'then'", "'where'", "'let'", "'in'", "'import'", 
		"'module'", "'data'", "'interface'", "'implements'", "'+'", "'-'", "'{'", 
		"'}'", "'*'", "'/'", "'<'", "'>'", "'<='", "'>='", "'=='", "'!='", "'='", 
		"'||'", "'|'", "'->'", "'::'", "','", "'.'", "'`'", "'&&'", "'('", "')'", 
		"'['", "']'", "';'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "IFKW", "ELSEKW", "THENKW", "WHEREKW", "LETKW", "INKW", "IMPORTKW", 
		"MODULEKW", "DATAKW", "INTERFACEKW", "IMPLEMENTSKW", "PLUS", "MINUS", 
		"LBRACE", "RBRACE", "MULT", "DIV", "LS", "GR", "LE", "GE", "EQ", "NOTEQ", 
		"ASSIGN", "OR", "DELIMETER", "FUNCTION_ARROW", "TYPEDOTS", "COMMA", "DOT", 
		"FUNCTION_TO_OPERATION", "AND", "LPAR", "RPAR", "LSQBR", "RSQBR", "SEMICOLON", 
		"BOOLEAN", "INT", "FLOAT", "STRING", "ID", "SKIP_"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "HosicoLang.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	private static String currentModuleName = "default";

	public static String getCurrentModuleName() { return currentModuleName; }

	public static void setCurrentModuleName(String newCurrentModuleName) { currentModuleName = newCurrentModuleName; }

	private final static Map<String, Map<String, DefinedFunction>> moduleMethods = new HashMap<>();

	public static Map<String, Map<String, DefinedFunction>> getModuleMethods() {
	    return Collections.unmodifiableMap(moduleMethods);
	}

	public static void clearModuleMethods() {
	    moduleMethods.clear();
	}

	public static void unsetCurrentModuleName() {
	    currentModuleName = "default";
	}

	public static class DefinedFunction {
	    public final DefinedFunction parent;
	    public final String name;

	    public DefinedFunction(String name, DefinedFunction parent) {
	        this.parent = parent;
	        this.name = name;
	    }
	}

	public HosicoLangParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public Token ID;
		public TerminalNode MODULEKW() { return getToken(HosicoLangParser.MODULEKW, 0); }
		public TerminalNode ID() { return getToken(HosicoLangParser.ID, 0); }
		public TerminalNode WHEREKW() { return getToken(HosicoLangParser.WHEREKW, 0); }
		public List<ImportStatementsContext> importStatements() {
			return getRuleContexts(ImportStatementsContext.class);
		}
		public ImportStatementsContext importStatements(int i) {
			return getRuleContext(ImportStatementsContext.class,i);
		}
		public List<FuncDefContext> funcDef() {
			return getRuleContexts(FuncDefContext.class);
		}
		public FuncDefContext funcDef(int i) {
			return getRuleContext(FuncDefContext.class,i);
		}
		public List<FuncTypeDefContext> funcTypeDef() {
			return getRuleContexts(FuncTypeDefContext.class);
		}
		public FuncTypeDefContext funcTypeDef(int i) {
			return getRuleContext(FuncTypeDefContext.class,i);
		}
		public List<DataDefContext> dataDef() {
			return getRuleContexts(DataDefContext.class);
		}
		public DataDefContext dataDef(int i) {
			return getRuleContext(DataDefContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(30);
			match(MODULEKW);
			setState(31);
			((ProgramContext)_localctx).ID = match(ID);
			setState(32);
			match(WHEREKW);
			setState(36);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==IMPORTKW) {
				{
				{
				setState(33);
				importStatements();
				}
				}
				setState(38);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(44);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==DATAKW || _la==ID) {
				{
				setState(42);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
				case 1:
					{
					setState(39);
					funcDef(true);
					}
					break;
				case 2:
					{
					setState(40);
					funcTypeDef();
					}
					break;
				case 3:
					{
					setState(41);
					dataDef();
					}
					break;
				}
				}
				setState(46);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

			setCurrentModuleName((((ProgramContext)_localctx).ID!=null?((ProgramContext)_localctx).ID.getText():null));

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportStatementsContext extends ParserRuleContext {
		public TerminalNode IMPORTKW() { return getToken(HosicoLangParser.IMPORTKW, 0); }
		public QualifiedIdContext qualifiedId() {
			return getRuleContext(QualifiedIdContext.class,0);
		}
		public TerminalNode LBRACE() { return getToken(HosicoLangParser.LBRACE, 0); }
		public List<TerminalNode> ID() { return getTokens(HosicoLangParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(HosicoLangParser.ID, i);
		}
		public TerminalNode RBRACE() { return getToken(HosicoLangParser.RBRACE, 0); }
		public List<TerminalNode> COMMA() { return getTokens(HosicoLangParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(HosicoLangParser.COMMA, i);
		}
		public ImportStatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importStatements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterImportStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitImportStatements(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitImportStatements(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImportStatementsContext importStatements() throws RecognitionException {
		ImportStatementsContext _localctx = new ImportStatementsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_importStatements);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(49);
			match(IMPORTKW);
			setState(50);
			qualifiedId();
			setState(62);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case EOF:
			case IMPORTKW:
			case DATAKW:
			case ID:
				{
				}
				break;
			case LBRACE:
				{
				setState(52);
				match(LBRACE);
				setState(53);
				match(ID);
				setState(58);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(54);
					match(COMMA);
					setState(55);
					match(ID);
					}
					}
					setState(60);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(61);
				match(RBRACE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncDefContext extends ParserRuleContext {
		public boolean isModuleMethod;
		public Token ID;
		public TerminalNode ASSIGN() { return getToken(HosicoLangParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(HosicoLangParser.SEMICOLON, 0); }
		public List<TerminalNode> ID() { return getTokens(HosicoLangParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(HosicoLangParser.ID, i);
		}
		public TerminalNode WHEREKW() { return getToken(HosicoLangParser.WHEREKW, 0); }
		public TerminalNode LBRACE() { return getToken(HosicoLangParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(HosicoLangParser.RBRACE, 0); }
		public List<FuncDefContext> funcDef() {
			return getRuleContexts(FuncDefContext.class);
		}
		public FuncDefContext funcDef(int i) {
			return getRuleContext(FuncDefContext.class,i);
		}
		public List<FuncTypeDefContext> funcTypeDef() {
			return getRuleContexts(FuncTypeDefContext.class);
		}
		public FuncTypeDefContext funcTypeDef(int i) {
			return getRuleContext(FuncTypeDefContext.class,i);
		}
		public FuncDefContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FuncDefContext(ParserRuleContext parent, int invokingState, boolean isModuleMethod) {
			super(parent, invokingState);
			this.isModuleMethod = isModuleMethod;
		}
		@Override public int getRuleIndex() { return RULE_funcDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterFuncDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitFuncDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitFuncDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncDefContext funcDef(boolean isModuleMethod) throws RecognitionException {
		FuncDefContext _localctx = new FuncDefContext(_ctx, getState(), isModuleMethod);
		enterRule(_localctx, 4, RULE_funcDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(64);
				((FuncDefContext)_localctx).ID = match(ID);
				}
				}
				setState(67); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID );
			setState(69);
			match(ASSIGN);
			setState(70);
			expression(0);
			setState(81);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WHEREKW) {
				{
				setState(71);
				match(WHEREKW);
				setState(72);
				match(LBRACE);
				setState(75); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					setState(75);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
					case 1:
						{
						setState(73);
						funcDef(false);
						}
						break;
					case 2:
						{
						setState(74);
						funcTypeDef();
						}
						break;
					}
					}
					setState(77); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(79);
				match(RBRACE);
				}
			}

			setState(83);
			match(SEMICOLON);

			if (!moduleMethods.containsKey(currentModuleName))
			    moduleMethods.put(currentModuleName, new HashMap<>());

			if (isModuleMethod) {
			    if (moduleMethods.get(currentModuleName).containsKey(((FuncDefContext)_localctx).ID().get(0).getText()))
			        throw new RuntimeException("Compilation error! Function can be defined once.");

			    moduleMethods.get(currentModuleName).put(((FuncDefContext)_localctx).ID().get(0).getText(), new DefinedFunction(((FuncDefContext)_localctx).ID().get(0).getText(), null));
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncTypeDefContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(HosicoLangParser.ID, 0); }
		public TerminalNode TYPEDOTS() { return getToken(HosicoLangParser.TYPEDOTS, 0); }
		public TypeExpressionContext typeExpression() {
			return getRuleContext(TypeExpressionContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(HosicoLangParser.SEMICOLON, 0); }
		public FuncTypeDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcTypeDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterFuncTypeDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitFuncTypeDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitFuncTypeDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncTypeDefContext funcTypeDef() throws RecognitionException {
		FuncTypeDefContext _localctx = new FuncTypeDefContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_funcTypeDef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			match(ID);
			setState(87);
			match(TYPEDOTS);
			setState(88);
			typeExpression(0);
			setState(89);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeExpressionContext extends ParserRuleContext {
		public TerminalNode LSQBR() { return getToken(HosicoLangParser.LSQBR, 0); }
		public List<TypeExpressionContext> typeExpression() {
			return getRuleContexts(TypeExpressionContext.class);
		}
		public TypeExpressionContext typeExpression(int i) {
			return getRuleContext(TypeExpressionContext.class,i);
		}
		public TerminalNode RSQBR() { return getToken(HosicoLangParser.RSQBR, 0); }
		public TerminalNode LPAR() { return getToken(HosicoLangParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(HosicoLangParser.RPAR, 0); }
		public QualifiedIdContext qualifiedId() {
			return getRuleContext(QualifiedIdContext.class,0);
		}
		public TerminalNode FUNCTION_ARROW() { return getToken(HosicoLangParser.FUNCTION_ARROW, 0); }
		public TypeExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterTypeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitTypeExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitTypeExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeExpressionContext typeExpression() throws RecognitionException {
		return typeExpression(0);
	}

	private TypeExpressionContext typeExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TypeExpressionContext _localctx = new TypeExpressionContext(_ctx, _parentState);
		TypeExpressionContext _prevctx = _localctx;
		int _startState = 8;
		enterRecursionRule(_localctx, 8, RULE_typeExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LSQBR:
				{
				setState(92);
				match(LSQBR);
				setState(93);
				typeExpression(0);
				setState(94);
				match(RSQBR);
				}
				break;
			case LPAR:
				{
				setState(96);
				match(LPAR);
				setState(97);
				typeExpression(0);
				setState(98);
				match(RPAR);
				}
				break;
			case ID:
				{
				setState(100);
				qualifiedId();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(108);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new TypeExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_typeExpression);
					setState(103);
					if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
					setState(104);
					match(FUNCTION_ARROW);
					setState(105);
					typeExpression(5);
					}
					} 
				}
				setState(110);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class QualifiedIdContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(HosicoLangParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(HosicoLangParser.ID, i);
		}
		public List<TerminalNode> DOT() { return getTokens(HosicoLangParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(HosicoLangParser.DOT, i);
		}
		public QualifiedIdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualifiedId; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterQualifiedId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitQualifiedId(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitQualifiedId(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QualifiedIdContext qualifiedId() throws RecognitionException {
		QualifiedIdContext _localctx = new QualifiedIdContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_qualifiedId);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			match(ID);
			setState(116);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(112);
					match(DOT);
					setState(113);
					match(ID);
					}
					} 
				}
				setState(118);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public TerminalNode LPAR() { return getToken(HosicoLangParser.LPAR, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode RPAR() { return getToken(HosicoLangParser.RPAR, 0); }
		public TerminalNode IFKW() { return getToken(HosicoLangParser.IFKW, 0); }
		public TerminalNode THENKW() { return getToken(HosicoLangParser.THENKW, 0); }
		public TerminalNode ELSEKW() { return getToken(HosicoLangParser.ELSEKW, 0); }
		public TerminalNode LETKW() { return getToken(HosicoLangParser.LETKW, 0); }
		public TerminalNode LBRACE() { return getToken(HosicoLangParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(HosicoLangParser.RBRACE, 0); }
		public TerminalNode INKW() { return getToken(HosicoLangParser.INKW, 0); }
		public List<FuncDefContext> funcDef() {
			return getRuleContexts(FuncDefContext.class);
		}
		public FuncDefContext funcDef(int i) {
			return getRuleContext(FuncDefContext.class,i);
		}
		public List<FuncTypeDefContext> funcTypeDef() {
			return getRuleContexts(FuncTypeDefContext.class);
		}
		public FuncTypeDefContext funcTypeDef(int i) {
			return getRuleContext(FuncTypeDefContext.class,i);
		}
		public TerminalNode MULT() { return getToken(HosicoLangParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(HosicoLangParser.DIV, 0); }
		public TerminalNode PLUS() { return getToken(HosicoLangParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(HosicoLangParser.MINUS, 0); }
		public List<TerminalNode> FUNCTION_TO_OPERATION() { return getTokens(HosicoLangParser.FUNCTION_TO_OPERATION); }
		public TerminalNode FUNCTION_TO_OPERATION(int i) {
			return getToken(HosicoLangParser.FUNCTION_TO_OPERATION, i);
		}
		public TerminalNode ID() { return getToken(HosicoLangParser.ID, 0); }
		public TerminalNode LS() { return getToken(HosicoLangParser.LS, 0); }
		public TerminalNode LE() { return getToken(HosicoLangParser.LE, 0); }
		public TerminalNode GR() { return getToken(HosicoLangParser.GR, 0); }
		public TerminalNode GE() { return getToken(HosicoLangParser.GE, 0); }
		public TerminalNode EQ() { return getToken(HosicoLangParser.EQ, 0); }
		public TerminalNode NOTEQ() { return getToken(HosicoLangParser.NOTEQ, 0); }
		public TerminalNode OR() { return getToken(HosicoLangParser.OR, 0); }
		public TerminalNode AND() { return getToken(HosicoLangParser.AND, 0); }
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 12;
		enterRecursionRule(_localctx, 12, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(145);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(120);
				simpleExpression();
				}
				break;
			case 2:
				{
				setState(121);
				functionCall();
				}
				break;
			case 3:
				{
				setState(122);
				match(LPAR);
				setState(123);
				expression(0);
				setState(124);
				match(RPAR);
				}
				break;
			case 4:
				{
				setState(126);
				match(IFKW);
				setState(127);
				expression(0);
				setState(128);
				match(THENKW);
				setState(129);
				expression(0);
				setState(130);
				match(ELSEKW);
				setState(131);
				expression(2);
				}
				break;
			case 5:
				{
				setState(133);
				match(LETKW);
				setState(134);
				match(LBRACE);
				setState(137); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					setState(137);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						setState(135);
						funcDef(false);
						}
						break;
					case 2:
						{
						setState(136);
						funcTypeDef();
						}
						break;
					}
					}
					setState(139); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ID );
				setState(141);
				match(RBRACE);
				setState(142);
				match(INKW);
				setState(143);
				expression(1);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(169);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(167);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
					case 1:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(147);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(148);
						_la = _input.LA(1);
						if ( !(_la==MULT || _la==DIV) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(149);
						expression(11);
						}
						break;
					case 2:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(150);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(151);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(152);
						expression(10);
						}
						break;
					case 3:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(153);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(154);
						match(FUNCTION_TO_OPERATION);
						setState(155);
						match(ID);
						setState(156);
						match(FUNCTION_TO_OPERATION);
						setState(157);
						expression(9);
						}
						break;
					case 4:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(158);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(159);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LS) | (1L << GR) | (1L << LE) | (1L << GE))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(160);
						expression(8);
						}
						break;
					case 5:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(161);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(162);
						_la = _input.LA(1);
						if ( !(_la==EQ || _la==NOTEQ) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(163);
						expression(7);
						}
						break;
					case 6:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(164);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(165);
						_la = _input.LA(1);
						if ( !(_la==OR || _la==AND) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(166);
						expression(6);
						}
						break;
					}
					} 
				}
				setState(171);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SimpleExpressionContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(HosicoLangParser.INT, 0); }
		public TerminalNode FLOAT() { return getToken(HosicoLangParser.FLOAT, 0); }
		public QualifiedIdContext qualifiedId() {
			return getRuleContext(QualifiedIdContext.class,0);
		}
		public TerminalNode STRING() { return getToken(HosicoLangParser.STRING, 0); }
		public TerminalNode BOOLEAN() { return getToken(HosicoLangParser.BOOLEAN, 0); }
		public SimpleExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterSimpleExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitSimpleExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitSimpleExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleExpressionContext simpleExpression() throws RecognitionException {
		SimpleExpressionContext _localctx = new SimpleExpressionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_simpleExpression);
		try {
			setState(177);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(172);
				match(INT);
				}
				break;
			case FLOAT:
				enterOuterAlt(_localctx, 2);
				{
				setState(173);
				match(FLOAT);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 3);
				{
				setState(174);
				qualifiedId();
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 4);
				{
				setState(175);
				match(STRING);
				}
				break;
			case BOOLEAN:
				enterOuterAlt(_localctx, 5);
				{
				setState(176);
				match(BOOLEAN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataDefContext extends ParserRuleContext {
		public TerminalNode DATAKW() { return getToken(HosicoLangParser.DATAKW, 0); }
		public TerminalNode ID() { return getToken(HosicoLangParser.ID, 0); }
		public TerminalNode ASSIGN() { return getToken(HosicoLangParser.ASSIGN, 0); }
		public List<CtorDefContext> ctorDef() {
			return getRuleContexts(CtorDefContext.class);
		}
		public CtorDefContext ctorDef(int i) {
			return getRuleContext(CtorDefContext.class,i);
		}
		public TerminalNode SEMICOLON() { return getToken(HosicoLangParser.SEMICOLON, 0); }
		public List<TerminalNode> DELIMETER() { return getTokens(HosicoLangParser.DELIMETER); }
		public TerminalNode DELIMETER(int i) {
			return getToken(HosicoLangParser.DELIMETER, i);
		}
		public DataDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterDataDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitDataDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitDataDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataDefContext dataDef() throws RecognitionException {
		DataDefContext _localctx = new DataDefContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_dataDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(179);
			match(DATAKW);
			setState(180);
			match(ID);
			setState(181);
			match(ASSIGN);
			setState(182);
			ctorDef();
			setState(187);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==DELIMETER) {
				{
				{
				setState(183);
				match(DELIMETER);
				setState(184);
				ctorDef();
				}
				}
				setState(189);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(190);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CtorDefContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(HosicoLangParser.ID, 0); }
		public TerminalNode LBRACE() { return getToken(HosicoLangParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(HosicoLangParser.RBRACE, 0); }
		public List<FieldDefContext> fieldDef() {
			return getRuleContexts(FieldDefContext.class);
		}
		public FieldDefContext fieldDef(int i) {
			return getRuleContext(FieldDefContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(HosicoLangParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(HosicoLangParser.COMMA, i);
		}
		public CtorDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ctorDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterCtorDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitCtorDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitCtorDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CtorDefContext ctorDef() throws RecognitionException {
		CtorDefContext _localctx = new CtorDefContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_ctorDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(ID);
			setState(193);
			match(LBRACE);
			setState(203);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case RBRACE:
				{
				}
				break;
			case ID:
				{
				setState(195);
				fieldDef();
				setState(200);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(196);
					match(COMMA);
					setState(197);
					fieldDef();
					}
					}
					setState(202);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(205);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldDefContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(HosicoLangParser.ID, 0); }
		public TerminalNode TYPEDOTS() { return getToken(HosicoLangParser.TYPEDOTS, 0); }
		public TypeExpressionContext typeExpression() {
			return getRuleContext(TypeExpressionContext.class,0);
		}
		public FieldDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterFieldDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitFieldDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitFieldDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FieldDefContext fieldDef() throws RecognitionException {
		FieldDefContext _localctx = new FieldDefContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_fieldDef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(207);
			match(ID);
			setState(208);
			match(TYPEDOTS);
			setState(209);
			typeExpression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public QualifiedIdContext qualifiedId() {
			return getRuleContext(QualifiedIdContext.class,0);
		}
		public List<ArgumentContext> argument() {
			return getRuleContexts(ArgumentContext.class);
		}
		public ArgumentContext argument(int i) {
			return getRuleContext(ArgumentContext.class,i);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitFunctionCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitFunctionCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_functionCall);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(211);
			qualifiedId();
			setState(215);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(212);
					argument();
					}
					} 
				}
				setState(217);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentContext extends ParserRuleContext {
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ArgumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterArgument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitArgument(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitArgument(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentContext argument() throws RecognitionException {
		ArgumentContext _localctx = new ArgumentContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_argument);
		try {
			setState(220);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(218);
				simpleExpression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(219);
				expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InterfaceDefContext extends ParserRuleContext {
		public TerminalNode INTERFACEKW() { return getToken(HosicoLangParser.INTERFACEKW, 0); }
		public TerminalNode ID() { return getToken(HosicoLangParser.ID, 0); }
		public TerminalNode LBRACE() { return getToken(HosicoLangParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(HosicoLangParser.RBRACE, 0); }
		public List<FuncTypeDefContext> funcTypeDef() {
			return getRuleContexts(FuncTypeDefContext.class);
		}
		public FuncTypeDefContext funcTypeDef(int i) {
			return getRuleContext(FuncTypeDefContext.class,i);
		}
		public InterfaceDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interfaceDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterInterfaceDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitInterfaceDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitInterfaceDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InterfaceDefContext interfaceDef() throws RecognitionException {
		InterfaceDefContext _localctx = new InterfaceDefContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_interfaceDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(222);
			match(INTERFACEKW);
			setState(223);
			match(ID);
			setState(224);
			match(LBRACE);
			setState(228);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID) {
				{
				{
				setState(225);
				funcTypeDef();
				}
				}
				setState(230);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(231);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImplementsDefContext extends ParserRuleContext {
		public List<QualifiedIdContext> qualifiedId() {
			return getRuleContexts(QualifiedIdContext.class);
		}
		public QualifiedIdContext qualifiedId(int i) {
			return getRuleContext(QualifiedIdContext.class,i);
		}
		public TerminalNode IMPLEMENTSKW() { return getToken(HosicoLangParser.IMPLEMENTSKW, 0); }
		public TerminalNode LBRACE() { return getToken(HosicoLangParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(HosicoLangParser.RBRACE, 0); }
		public List<FuncDefContext> funcDef() {
			return getRuleContexts(FuncDefContext.class);
		}
		public FuncDefContext funcDef(int i) {
			return getRuleContext(FuncDefContext.class,i);
		}
		public ImplementsDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_implementsDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).enterImplementsDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HosicoLangListener ) ((HosicoLangListener)listener).exitImplementsDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HosicoLangVisitor ) return ((HosicoLangVisitor<? extends T>)visitor).visitImplementsDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImplementsDefContext implementsDef() throws RecognitionException {
		ImplementsDefContext _localctx = new ImplementsDefContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_implementsDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(233);
			qualifiedId();
			setState(234);
			match(IMPLEMENTSKW);
			setState(235);
			qualifiedId();
			setState(236);
			match(LBRACE);
			setState(240);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID) {
				{
				{
				setState(237);
				funcDef(true);
				}
				}
				setState(242);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(243);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 4:
			return typeExpression_sempred((TypeExpressionContext)_localctx, predIndex);
		case 6:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean typeExpression_sempred(TypeExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 10);
		case 2:
			return precpred(_ctx, 9);
		case 3:
			return precpred(_ctx, 8);
		case 4:
			return precpred(_ctx, 7);
		case 5:
			return precpred(_ctx, 6);
		case 6:
			return precpred(_ctx, 5);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3-\u00f8\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\2\3\2\7\2"+
		"%\n\2\f\2\16\2(\13\2\3\2\3\2\3\2\7\2-\n\2\f\2\16\2\60\13\2\3\2\3\2\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\7\3;\n\3\f\3\16\3>\13\3\3\3\5\3A\n\3\3\4\6\4"+
		"D\n\4\r\4\16\4E\3\4\3\4\3\4\3\4\3\4\3\4\6\4N\n\4\r\4\16\4O\3\4\3\4\5\4"+
		"T\n\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\5\6h\n\6\3\6\3\6\3\6\7\6m\n\6\f\6\16\6p\13\6\3\7\3\7\3\7\7\7"+
		"u\n\7\f\7\16\7x\13\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\6\b\u008c\n\b\r\b\16\b\u008d\3\b\3\b\3\b\3\b\5"+
		"\b\u0094\n\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\7\b\u00aa\n\b\f\b\16\b\u00ad\13\b\3\t\3\t\3\t\3"+
		"\t\3\t\5\t\u00b4\n\t\3\n\3\n\3\n\3\n\3\n\3\n\7\n\u00bc\n\n\f\n\16\n\u00bf"+
		"\13\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u00c9\n\13\f\13\16\13"+
		"\u00cc\13\13\5\13\u00ce\n\13\3\13\3\13\3\f\3\f\3\f\3\f\3\r\3\r\7\r\u00d8"+
		"\n\r\f\r\16\r\u00db\13\r\3\16\3\16\5\16\u00df\n\16\3\17\3\17\3\17\3\17"+
		"\7\17\u00e5\n\17\f\17\16\17\u00e8\13\17\3\17\3\17\3\20\3\20\3\20\3\20"+
		"\3\20\7\20\u00f1\n\20\f\20\16\20\u00f4\13\20\3\20\3\20\3\20\2\4\n\16\21"+
		"\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36\2\7\3\2\22\23\3\2\16\17\3\2\24"+
		"\27\3\2\30\31\4\2\33\33\"\"\2\u010d\2 \3\2\2\2\4\63\3\2\2\2\6C\3\2\2\2"+
		"\bX\3\2\2\2\ng\3\2\2\2\fq\3\2\2\2\16\u0093\3\2\2\2\20\u00b3\3\2\2\2\22"+
		"\u00b5\3\2\2\2\24\u00c2\3\2\2\2\26\u00d1\3\2\2\2\30\u00d5\3\2\2\2\32\u00de"+
		"\3\2\2\2\34\u00e0\3\2\2\2\36\u00eb\3\2\2\2 !\7\n\2\2!\"\7,\2\2\"&\7\6"+
		"\2\2#%\5\4\3\2$#\3\2\2\2%(\3\2\2\2&$\3\2\2\2&\'\3\2\2\2\'.\3\2\2\2(&\3"+
		"\2\2\2)-\5\6\4\2*-\5\b\5\2+-\5\22\n\2,)\3\2\2\2,*\3\2\2\2,+\3\2\2\2-\60"+
		"\3\2\2\2.,\3\2\2\2./\3\2\2\2/\61\3\2\2\2\60.\3\2\2\2\61\62\b\2\1\2\62"+
		"\3\3\2\2\2\63\64\7\t\2\2\64@\5\f\7\2\65A\3\2\2\2\66\67\7\20\2\2\67<\7"+
		",\2\289\7\37\2\29;\7,\2\2:8\3\2\2\2;>\3\2\2\2<:\3\2\2\2<=\3\2\2\2=?\3"+
		"\2\2\2><\3\2\2\2?A\7\21\2\2@\65\3\2\2\2@\66\3\2\2\2A\5\3\2\2\2BD\7,\2"+
		"\2CB\3\2\2\2DE\3\2\2\2EC\3\2\2\2EF\3\2\2\2FG\3\2\2\2GH\7\32\2\2HS\5\16"+
		"\b\2IJ\7\6\2\2JM\7\20\2\2KN\5\6\4\2LN\5\b\5\2MK\3\2\2\2ML\3\2\2\2NO\3"+
		"\2\2\2OM\3\2\2\2OP\3\2\2\2PQ\3\2\2\2QR\7\21\2\2RT\3\2\2\2SI\3\2\2\2ST"+
		"\3\2\2\2TU\3\2\2\2UV\7\'\2\2VW\b\4\1\2W\7\3\2\2\2XY\7,\2\2YZ\7\36\2\2"+
		"Z[\5\n\6\2[\\\7\'\2\2\\\t\3\2\2\2]^\b\6\1\2^_\7%\2\2_`\5\n\6\2`a\7&\2"+
		"\2ah\3\2\2\2bc\7#\2\2cd\5\n\6\2de\7$\2\2eh\3\2\2\2fh\5\f\7\2g]\3\2\2\2"+
		"gb\3\2\2\2gf\3\2\2\2hn\3\2\2\2ij\f\6\2\2jk\7\35\2\2km\5\n\6\7li\3\2\2"+
		"\2mp\3\2\2\2nl\3\2\2\2no\3\2\2\2o\13\3\2\2\2pn\3\2\2\2qv\7,\2\2rs\7 \2"+
		"\2su\7,\2\2tr\3\2\2\2ux\3\2\2\2vt\3\2\2\2vw\3\2\2\2w\r\3\2\2\2xv\3\2\2"+
		"\2yz\b\b\1\2z\u0094\5\20\t\2{\u0094\5\30\r\2|}\7#\2\2}~\5\16\b\2~\177"+
		"\7$\2\2\177\u0094\3\2\2\2\u0080\u0081\7\3\2\2\u0081\u0082\5\16\b\2\u0082"+
		"\u0083\7\5\2\2\u0083\u0084\5\16\b\2\u0084\u0085\7\4\2\2\u0085\u0086\5"+
		"\16\b\4\u0086\u0094\3\2\2\2\u0087\u0088\7\7\2\2\u0088\u008b\7\20\2\2\u0089"+
		"\u008c\5\6\4\2\u008a\u008c\5\b\5\2\u008b\u0089\3\2\2\2\u008b\u008a\3\2"+
		"\2\2\u008c\u008d\3\2\2\2\u008d\u008b\3\2\2\2\u008d\u008e\3\2\2\2\u008e"+
		"\u008f\3\2\2\2\u008f\u0090\7\21\2\2\u0090\u0091\7\b\2\2\u0091\u0092\5"+
		"\16\b\3\u0092\u0094\3\2\2\2\u0093y\3\2\2\2\u0093{\3\2\2\2\u0093|\3\2\2"+
		"\2\u0093\u0080\3\2\2\2\u0093\u0087\3\2\2\2\u0094\u00ab\3\2\2\2\u0095\u0096"+
		"\f\f\2\2\u0096\u0097\t\2\2\2\u0097\u00aa\5\16\b\r\u0098\u0099\f\13\2\2"+
		"\u0099\u009a\t\3\2\2\u009a\u00aa\5\16\b\f\u009b\u009c\f\n\2\2\u009c\u009d"+
		"\7!\2\2\u009d\u009e\7,\2\2\u009e\u009f\7!\2\2\u009f\u00aa\5\16\b\13\u00a0"+
		"\u00a1\f\t\2\2\u00a1\u00a2\t\4\2\2\u00a2\u00aa\5\16\b\n\u00a3\u00a4\f"+
		"\b\2\2\u00a4\u00a5\t\5\2\2\u00a5\u00aa\5\16\b\t\u00a6\u00a7\f\7\2\2\u00a7"+
		"\u00a8\t\6\2\2\u00a8\u00aa\5\16\b\b\u00a9\u0095\3\2\2\2\u00a9\u0098\3"+
		"\2\2\2\u00a9\u009b\3\2\2\2\u00a9\u00a0\3\2\2\2\u00a9\u00a3\3\2\2\2\u00a9"+
		"\u00a6\3\2\2\2\u00aa\u00ad\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ab\u00ac\3\2"+
		"\2\2\u00ac\17\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ae\u00b4\7)\2\2\u00af\u00b4"+
		"\7*\2\2\u00b0\u00b4\5\f\7\2\u00b1\u00b4\7+\2\2\u00b2\u00b4\7(\2\2\u00b3"+
		"\u00ae\3\2\2\2\u00b3\u00af\3\2\2\2\u00b3\u00b0\3\2\2\2\u00b3\u00b1\3\2"+
		"\2\2\u00b3\u00b2\3\2\2\2\u00b4\21\3\2\2\2\u00b5\u00b6\7\13\2\2\u00b6\u00b7"+
		"\7,\2\2\u00b7\u00b8\7\32\2\2\u00b8\u00bd\5\24\13\2\u00b9\u00ba\7\34\2"+
		"\2\u00ba\u00bc\5\24\13\2\u00bb\u00b9\3\2\2\2\u00bc\u00bf\3\2\2\2\u00bd"+
		"\u00bb\3\2\2\2\u00bd\u00be\3\2\2\2\u00be\u00c0\3\2\2\2\u00bf\u00bd\3\2"+
		"\2\2\u00c0\u00c1\7\'\2\2\u00c1\23\3\2\2\2\u00c2\u00c3\7,\2\2\u00c3\u00cd"+
		"\7\20\2\2\u00c4\u00ce\3\2\2\2\u00c5\u00ca\5\26\f\2\u00c6\u00c7\7\37\2"+
		"\2\u00c7\u00c9\5\26\f\2\u00c8\u00c6\3\2\2\2\u00c9\u00cc\3\2\2\2\u00ca"+
		"\u00c8\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb\u00ce\3\2\2\2\u00cc\u00ca\3\2"+
		"\2\2\u00cd\u00c4\3\2\2\2\u00cd\u00c5\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf"+
		"\u00d0\7\21\2\2\u00d0\25\3\2\2\2\u00d1\u00d2\7,\2\2\u00d2\u00d3\7\36\2"+
		"\2\u00d3\u00d4\5\n\6\2\u00d4\27\3\2\2\2\u00d5\u00d9\5\f\7\2\u00d6\u00d8"+
		"\5\32\16\2\u00d7\u00d6\3\2\2\2\u00d8\u00db\3\2\2\2\u00d9\u00d7\3\2\2\2"+
		"\u00d9\u00da\3\2\2\2\u00da\31\3\2\2\2\u00db\u00d9\3\2\2\2\u00dc\u00df"+
		"\5\20\t\2\u00dd\u00df\5\16\b\2\u00de\u00dc\3\2\2\2\u00de\u00dd\3\2\2\2"+
		"\u00df\33\3\2\2\2\u00e0\u00e1\7\f\2\2\u00e1\u00e2\7,\2\2\u00e2\u00e6\7"+
		"\20\2\2\u00e3\u00e5\5\b\5\2\u00e4\u00e3\3\2\2\2\u00e5\u00e8\3\2\2\2\u00e6"+
		"\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7\u00e9\3\2\2\2\u00e8\u00e6\3\2"+
		"\2\2\u00e9\u00ea\7\21\2\2\u00ea\35\3\2\2\2\u00eb\u00ec\5\f\7\2\u00ec\u00ed"+
		"\7\r\2\2\u00ed\u00ee\5\f\7\2\u00ee\u00f2\7\20\2\2\u00ef\u00f1\5\6\4\2"+
		"\u00f0\u00ef\3\2\2\2\u00f1\u00f4\3\2\2\2\u00f2\u00f0\3\2\2\2\u00f2\u00f3"+
		"\3\2\2\2\u00f3\u00f5\3\2\2\2\u00f4\u00f2\3\2\2\2\u00f5\u00f6\7\21\2\2"+
		"\u00f6\37\3\2\2\2\33&,.<@EMOSgnv\u008b\u008d\u0093\u00a9\u00ab\u00b3\u00bd"+
		"\u00ca\u00cd\u00d9\u00de\u00e6\u00f2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}