// Generated from /home/sanusha/Proga/lazyjava/HosicoLang.g4 by ANTLR 4.7
package com.gitlab.zinoviy23.hosicolang.antlr;

import java.util.*;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link HosicoLangParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface HosicoLangVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(HosicoLangParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#importStatements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportStatements(HosicoLangParser.ImportStatementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#funcDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncDef(HosicoLangParser.FuncDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#funcTypeDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncTypeDef(HosicoLangParser.FuncTypeDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#typeExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeExpression(HosicoLangParser.TypeExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#qualifiedId}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualifiedId(HosicoLangParser.QualifiedIdContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(HosicoLangParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#simpleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleExpression(HosicoLangParser.SimpleExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#dataDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataDef(HosicoLangParser.DataDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#ctorDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtorDef(HosicoLangParser.CtorDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#fieldDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFieldDef(HosicoLangParser.FieldDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#functionCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(HosicoLangParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument(HosicoLangParser.ArgumentContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#interfaceDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterfaceDef(HosicoLangParser.InterfaceDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link HosicoLangParser#implementsDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImplementsDef(HosicoLangParser.ImplementsDefContext ctx);
}