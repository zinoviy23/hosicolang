// Generated from /home/sanusha/Proga/lazyjava/HosicoLang.g4 by ANTLR 4.7
package com.gitlab.zinoviy23.hosicolang.antlr;

import java.util.*;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link HosicoLangParser}.
 */
public interface HosicoLangListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(HosicoLangParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(HosicoLangParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#importStatements}.
	 * @param ctx the parse tree
	 */
	void enterImportStatements(HosicoLangParser.ImportStatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#importStatements}.
	 * @param ctx the parse tree
	 */
	void exitImportStatements(HosicoLangParser.ImportStatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#funcDef}.
	 * @param ctx the parse tree
	 */
	void enterFuncDef(HosicoLangParser.FuncDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#funcDef}.
	 * @param ctx the parse tree
	 */
	void exitFuncDef(HosicoLangParser.FuncDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#funcTypeDef}.
	 * @param ctx the parse tree
	 */
	void enterFuncTypeDef(HosicoLangParser.FuncTypeDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#funcTypeDef}.
	 * @param ctx the parse tree
	 */
	void exitFuncTypeDef(HosicoLangParser.FuncTypeDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#typeExpression}.
	 * @param ctx the parse tree
	 */
	void enterTypeExpression(HosicoLangParser.TypeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#typeExpression}.
	 * @param ctx the parse tree
	 */
	void exitTypeExpression(HosicoLangParser.TypeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#qualifiedId}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedId(HosicoLangParser.QualifiedIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#qualifiedId}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedId(HosicoLangParser.QualifiedIdContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(HosicoLangParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(HosicoLangParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#simpleExpression}.
	 * @param ctx the parse tree
	 */
	void enterSimpleExpression(HosicoLangParser.SimpleExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#simpleExpression}.
	 * @param ctx the parse tree
	 */
	void exitSimpleExpression(HosicoLangParser.SimpleExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#dataDef}.
	 * @param ctx the parse tree
	 */
	void enterDataDef(HosicoLangParser.DataDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#dataDef}.
	 * @param ctx the parse tree
	 */
	void exitDataDef(HosicoLangParser.DataDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#ctorDef}.
	 * @param ctx the parse tree
	 */
	void enterCtorDef(HosicoLangParser.CtorDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#ctorDef}.
	 * @param ctx the parse tree
	 */
	void exitCtorDef(HosicoLangParser.CtorDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#fieldDef}.
	 * @param ctx the parse tree
	 */
	void enterFieldDef(HosicoLangParser.FieldDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#fieldDef}.
	 * @param ctx the parse tree
	 */
	void exitFieldDef(HosicoLangParser.FieldDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(HosicoLangParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(HosicoLangParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(HosicoLangParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(HosicoLangParser.ArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#interfaceDef}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceDef(HosicoLangParser.InterfaceDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#interfaceDef}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceDef(HosicoLangParser.InterfaceDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link HosicoLangParser#implementsDef}.
	 * @param ctx the parse tree
	 */
	void enterImplementsDef(HosicoLangParser.ImplementsDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link HosicoLangParser#implementsDef}.
	 * @param ctx the parse tree
	 */
	void exitImplementsDef(HosicoLangParser.ImplementsDefContext ctx);
}