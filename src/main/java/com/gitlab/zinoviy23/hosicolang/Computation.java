package com.gitlab.zinoviy23.hosicolang;

@FunctionalInterface
public interface Computation<T> {
    T compute();
}
