grammar HosicoLang;

@parser::header {
import java.util.*;
}

@parser::members {
private static String currentModuleName = "default";

public static String getCurrentModuleName() { return currentModuleName; }

public static void setCurrentModuleName(String newCurrentModuleName) { currentModuleName = newCurrentModuleName; }

private final static Map<String, Map<String, DefinedFunction>> moduleMethods = new HashMap<>();

public static Map<String, Map<String, DefinedFunction>> getModuleMethods() {
    return Collections.unmodifiableMap(moduleMethods);
}

public static void clearModuleMethods() {
    moduleMethods.clear();
}

public static void unsetCurrentModuleName() {
    currentModuleName = "default";
}

public static class DefinedFunction {
    public final DefinedFunction parent;
    public final String name;

    public DefinedFunction(String name, DefinedFunction parent) {
        this.parent = parent;
        this.name = name;
    }
}
}

program: MODULEKW ID WHEREKW importStatements* (funcDef[true]|funcTypeDef|dataDef)* {
setCurrentModuleName($ID.text);
};

importStatements: IMPORTKW qualifiedId (|LBRACE ID (COMMA ID)* RBRACE);

funcDef[boolean isModuleMethod]: ID+ ASSIGN  expression (WHEREKW LBRACE (funcDef[false]|funcTypeDef)+ RBRACE)? SEMICOLON {
if (!moduleMethods.containsKey(currentModuleName))
    moduleMethods.put(currentModuleName, new HashMap<>());

if (isModuleMethod) {
    if (moduleMethods.get(currentModuleName).containsKey($ID().get(0).getText()))
        throw new RuntimeException("Compilation error! Function can be defined once.");

    moduleMethods.get(currentModuleName).put($ID().get(0).getText(), new DefinedFunction($ID().get(0).getText(), null));
}
};

funcTypeDef: ID TYPEDOTS typeExpression SEMICOLON;

typeExpression:
    typeExpression FUNCTION_ARROW typeExpression |
    LSQBR typeExpression RSQBR |
    LPAR typeExpression RPAR |
    qualifiedId;

qualifiedId: ID (DOT ID)*;

expression:
    simpleExpression |
    expression (MULT | DIV) expression |
    expression (PLUS | MINUS) expression |
    expression FUNCTION_TO_OPERATION ID FUNCTION_TO_OPERATION expression |
    expression (LS | LE | GR | GE) expression |
    expression (EQ | NOTEQ) expression |
    expression (OR | AND) expression | functionCall |
    LPAR expression RPAR |
    IFKW expression THENKW expression ELSEKW expression |
    LETKW LBRACE (funcDef[false]|funcTypeDef)+ RBRACE INKW expression;

simpleExpression: INT | FLOAT | qualifiedId | STRING | BOOLEAN;

dataDef: DATAKW ID ASSIGN ctorDef (DELIMETER ctorDef)* SEMICOLON;
ctorDef: ID LBRACE (|fieldDef (COMMA fieldDef)*) RBRACE;
fieldDef: ID TYPEDOTS typeExpression;

functionCall: qualifiedId argument*;
argument: simpleExpression | expression;

interfaceDef: INTERFACEKW ID LBRACE funcTypeDef* RBRACE;

implementsDef: qualifiedId IMPLEMENTSKW qualifiedId LBRACE funcDef[true]* RBRACE;

IFKW: 'if';
ELSEKW: 'else';
THENKW: 'then';
WHEREKW: 'where';
LETKW: 'let';
INKW: 'in';
IMPORTKW: 'import';
MODULEKW: 'module';
DATAKW: 'data';
INTERFACEKW: 'interface';
IMPLEMENTSKW: 'implements';
PLUS: '+';
MINUS: '-';
LBRACE: '{';
RBRACE: '}';
MULT: '*';
DIV: '/';
LS: '<';
GR: '>';
LE: '<=';
GE: '>=';
EQ: '==';
NOTEQ: '!=';
ASSIGN: '=';
OR: '||';
DELIMETER: '|';
FUNCTION_ARROW: '->';
TYPEDOTS: '::';
COMMA: ',';
DOT: '.';
FUNCTION_TO_OPERATION: '`';
AND: '&&';
LPAR : '(';
RPAR : ')';
LSQBR : '[';
RSQBR : ']';
SEMICOLON: ';';
BOOLEAN : ('true'|'false');
INT : ('+'|'-')?([1-9][0-9]*|'0');
FLOAT : ('+'|'-')?([1-9][0-9]*|'0')?'.'[0-9]+;
STRING : '"'~["\n\r]*'"';
ID : [a-zA-Z_][a-zA-Z_0-9]*;
SKIP_: (WS | COMMENT) -> skip;

fragment COMMENT : '#'~[\r\n\f]*;
fragment WS : [ \n\r\t]+;